package ProjectConsiste;

import java.util.Locale;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import entities.People;

public class Program {

	public static void main(String[] args) {
		
		String path = "C:\\temp\\dataset.CSV";
		
		List<People> list = new ArrayList<People>(5);
		
		try (BufferedReader br = new BufferedReader(new FileReader(path))){
			
			String line = br.readLine();
			line = br.readLine();
			while(line != null) {
				
				
				String[] vect = line.split(",");
				String name = vect[0];
				String surname = vect[1];
				Double peso = Double.parseDouble(vect[2]);
				Double altura = Double.parseDouble(vect[3]);
				
				
				People peop = new People(name, surname, peso, altura);
				list.add(peop);
				
				line = br.readLine();
			}	
			System.out.println("Usuario:");
			for (People p : list) {
				System.out.println(p);
			}
		}
		catch (IOException e) {
			System.out.println("Erro: " + e.getMessage());
		}
	}
}

